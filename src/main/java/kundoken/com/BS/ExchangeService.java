package kundoken.com.BS;

public interface ExchangeService {

    public abstract float exchangeFactor (String fromCurr, String toCurr);
}
