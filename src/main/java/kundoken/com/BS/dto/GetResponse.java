package kundoken.com.BS.dto;


public class GetResponse {

    private final long id;
    private final String fromCurr;
    private final String toCurr;
    private final String amount;

    public GetResponse(long id, String fromCurr, String toCurr, float amount) {
        this.id = id;
        this.fromCurr = fromCurr;
        this.toCurr = toCurr;
        this.amount = String.format("%.2f", amount);
    }

    public long getId() {
        return id;
    }

    public String getFromCurr() {
        return fromCurr;
    }

    public String getToCurr() {
        return toCurr;
    }

    public String getAmount() {
        return amount;
    }
}
