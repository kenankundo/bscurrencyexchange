package kundoken.com.BS;

import kundoken.com.BS.dto.GetResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;


@RestController
public class ExchangeRestController {
    private static final String from = "from the Currency, %s!";
    private static final String to = "to the Currency, %s!";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    ExchangeCalculatorService exchangeCalculatorService;

    //http://localhost:8080/exchange?fromCurr=euro&toCurr=dollar&amount=500.213
    @GetMapping("/exchange")
    public GetResponse getResponse(
            @RequestParam(value = "fromCurr", defaultValue = "FROM") String fromCurr,
            @RequestParam(value = "toCurr", defaultValue = "TO") String toCurr,
            @RequestParam(value = "amount", defaultValue = "0.00") float amount) {
        return new GetResponse(counter.incrementAndGet(), String.format(from, fromCurr), String.format(to, toCurr),
                exchangeCalculatorService.exchangeFactor(fromCurr, toCurr) * amount);
    }
}
