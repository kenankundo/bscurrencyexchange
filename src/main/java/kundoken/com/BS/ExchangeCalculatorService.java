package kundoken.com.BS;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ExchangeCalculatorService implements ExchangeService{

    Map<String, Float> exchangeMap = new HashMap<String, Float>(){
        {
            put("euro", 1f);
            put("dollar", 1.2f);
            put("yen", 124.03f);
            put("won", 1312.16f);
            put("zimbabwe", 428.47f);
        }
    };

    @Override
    public float exchangeFactor (String fromCurr, String toCurr){
      if(exchangeMap.containsKey(fromCurr) && exchangeMap.containsKey(toCurr)){
          return exchangeMap.get(toCurr) / exchangeMap.get(fromCurr);
      } else return 1.00f;
    }


    
}
